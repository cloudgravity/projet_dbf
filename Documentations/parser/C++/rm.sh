#!/bin/bash
# Script pour supprimer les fichiers créés par le MakeFile car je ne comprend pas pourquoi ils ne sont pas supprimé dans le clean.
rm *.yy.cc
rm *.output
rm calc_parser.tab.cc
rm calc_parser.tab.hh 
rm calc_driver.o 
rm location.hh 
rm main.o
rm position.hh 
rm parser.o 
rm stack.hh 
rm lexer.o 
