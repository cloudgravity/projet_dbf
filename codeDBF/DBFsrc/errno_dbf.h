/**
 * Class where you can undestand our own errno
 */

#ifndef PROJET_DBF_ERRNO_DBF_H
#define PROJET_DBF_ERRNO_DBF_H

#define ERRNO_DBF_OK 0

//Error linked to socket
#define ERRNO_DBF_ERROR_SOCKET_CLOSED -1
#define ERRNO_DBF_ERROR_TO_READ_MESSAGE -2
#define ERRNO_DBF_ERROR_TO_SEND_MESSAGE -3

#endif //PROJET_DBF_ERRNO_DBF_H
