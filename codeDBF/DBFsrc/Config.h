//
// Created by baptiste on 20/03/17.
//

#ifndef PROJET_DBF_CONFIG_H
#define PROJET_DBF_CONFIG_H

#endif //PROJET_DBF_CONFIG_H


#include <iostream>
#define addressBdd "127.0.0.1"

class Config {
public:

    static const int portBdd = 3306;
    static const int portDBF = 3305;

};