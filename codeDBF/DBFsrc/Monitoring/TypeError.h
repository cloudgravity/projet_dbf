//
// Created by baptiste on 21/03/17.
//

#ifndef PROJET_DBF_TYPEERROR_H
#define PROJET_DBF_TYPEERROR_H


enum class TypeError {
    ERROR,
    WARNING,
    DANGER,
    INFO,
};


#endif //PROJET_DBF_TYPEERROR_H
